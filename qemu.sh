#!/bin/sh

qemu-system-x86_64 \
    -enable-kvm \
    -smp 2 \
    -m 1024 \
    -kernel arch/x86/boot/bzImage \
    -append "console=ttyS0 root=/dev/sda" \
    -nographic \
    -no-reboot \
    -drive file=wheezy.img,format=raw \
    -net nic -net user,hostfwd=tcp::10022-:22
